#!/usr/bin/env node

import dotenv from "dotenv";
dotenv.config();

import axios from "axios";

const TOKEN = process.env.TOKEN;

axios
  .get("http://localhost:3000/ok", {
    headers: { authentication: TOKEN },
  })
  .then(({ data }) => console.log("then", data))
  .catch(({ response }) =>
    console.log("catch", response?.data || response || "unknown error")
  );

axios
  .post(
    "http://localhost:3000/register",
    {
      user: "jane.doe",
      name: "Jane Doe",
      pass: "zxfciiM2Am67Rlk6F2Ezjtgel8po4km4CnGQws",
      email: "jane@doe.com",
    },
    {
      headers: { authentication: TOKEN },
    }
  )
  .then(({ data }) => console.log(data))
  .catch(({ response }) => console.log(response.data));
