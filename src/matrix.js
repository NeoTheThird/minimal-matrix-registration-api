"use strict";

/**
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import dotenv from "dotenv";
dotenv.config();

import axios from "axios";

const BASE = "https://matrix.medsafe.im";
const BOT_USER = process.env.BOT_USER;
const BOT_PASS = process.env.BOT_PASS;

if (!BOT_USER || !BOT_PASS) {
  console.error("Make sure BOT_USER and BOT_PASS env vars are set");
  process.exit(1);
}

const adminApi = axios.create({
  baseURL: `${BASE}/_synapse/admin/v2/`,
  timeout: 3000,
});
const clientApi = axios.create({
  baseURL: `${BASE}/_matrix/client/r0/`,
  timeout: 3000,
});

const getAccessToken = (user, pass) =>
  clientApi
    .post("/login", {
      type: "m.login.password",
      user: user,
      password: pass,
    })
    .then(({ data }) => data.access_token);

/**
 * connection to matrix.medsafe.im ok?
 * @returns {Promise<Boolean>}
 */
export const ok = () =>
  getAccessToken(BOT_USER, BOT_PASS)
    .then((token) => !!token)
    .catch(false);

const getUser = (user, access_token) =>
  adminApi
    .get(`/users/@${user}:medsafe.im`, {
      headers: { Authorization: `Bearer ${access_token}` },
    })
    .then(({ data }) => data);

const userExists = (user, access_token) =>
  getUser(user, access_token)
    .then((user) => true)
    .catch((error) => {
      if (error.response.status === 404) return false;
      else throw error;
    });

/**
 * register a user
 * @param {Object} param0 user metadata
 * @returns {Promise}
 */
export const register = ({ user, name, pass, email }) =>
  getAccessToken(BOT_USER, BOT_PASS)
    .then((access_token) =>
      userExists(user, access_token).then((exists) =>
        exists
          ? Promise.reject(new Error(`user ${user} exists`))
          : adminApi.put(
              `/users/@${user}:medsafe.im`,
              {
                password: pass,
                displayname: name,
                threepids: [{ medium: "email", address: email }],
              },
              {
                headers: { Authorization: `Bearer ${access_token}` },
              }
            )
      )
    )
    .then(({ data }) => data);
