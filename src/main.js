"use strict";

/**
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ok, register } from "./matrix.js";
import express from "express";
import bodyParser from "body-parser";
import { RateLimiterMemory } from "rate-limiter-flexible";

const TOKEN = process.env.TOKEN;
const PORT = process.env.PORT || 3000;

const app = express();
app.use(bodyParser.json());

const rateLimiter = new RateLimiterMemory({
  points: 5,
  duration: 2,
  blockDuration: 1,
});
const rateLimiterMiddleware = (req, res, next) =>
  rateLimiter
    .consume(req.ip)
    .then(() => next())
    .catch(() => res.status(429).send("Too Many Requests"));
app.use(rateLimiterMiddleware);

const corsMiddleware = (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, authentication"
  );
  next();
};
app.use(corsMiddleware);

app.get("/", (req, res) => res.redirect(302, process.env.FRONTEND_URL));

app.get("/ok", (req, res) =>
  !req.headers.authentication
    ? res.sendStatus(401)
    : req.headers.authentication !== TOKEN
    ? res.sendStatus(403)
    : ok().then((ok) => (ok ? res.sendStatus(200) : res.sendStatus(500)))
);

app.post("/register", (req, res) => {
  !req.headers.authentication
    ? res.sendStatus(401)
    : req.headers.authentication !== TOKEN
    ? res.sendStatus(403)
    : register(req.body)
        .then((user) => res.send(user))
        .catch((error) => {
          console.log(error);
          res.sendStatus(500);
        });
});

app.listen(PORT, () => {
  console.log(
    `${process.env.npm_package_name} listening at http://localhost:${PORT}`
  );
});
