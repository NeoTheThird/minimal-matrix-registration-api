# minimal-matrix-registration-api

A minimal express server that provides a REST endpoint for registering users on matrix servers. Originally created for use in [medsafe.im](https://medsafe.im).

## API

Set the `authentication` header to the string defined in the `TOKEN` env var.

### Response codes

| Status                      | Description                     |
|-----------------------------|---------------------------------|
| `200 OK`                    | alrighty!                       |
| `401 Unauthorized`          | `authentication` header missing |
| `403 Forbidden`             | `authentication` header invalid |
| `429 Too Many Requests`     | slow down! one call per second! |
| `500 Internal Server Error` | ono! something went wrong :'(   |

### Endpoints

#### GET `/ok`

Check authentication and connection to synapse.

#### POST `/register`

Create a user specified in `data`.

```javascript
{
  user: "jane.doe",
  name: "Jane Doe",
  pass: "6F2Ezjtgeql8po4kmr67Rlk4CnGQwszxfciiM2Am",
  email: "jane@doe.com",
}
```

## Setup

### Production

#### CapRover

Deploy to your [CapRover](https://caprover.com/) self-hosted PaaS by running `caprover deploy`. Set the env vars described below in the admin UI and be sure to enable and enforce HTTPS.

#### Docker

Build and start the `Dockerfile` with the env vars described below.

#### NodeJS

If you want, you can always deploy the repository as you would any other NodeJS server using the `src/main.js` script.

### Development

1. Clone the repository
2. `npm install` the dependencies
3. Create a `.env` file with the variables described below
4. `npm start` the server on port `3000`

## Environment variables

The server uses the followint env vars:

| Env Var        | Description         | Example                   |
|----------------|---------------------|---------------------------|
| `SYNAPSE_URL`  | Base url of the API | https://matrix.medsafe.im |
| `BOT_USER`     | Bot user username   | bot                       |
| `BOT_PASS`     | Bot user password   | `asdf`                    |
| `TOKEN`        | API token to accept | `1337`                    |
| `FRONTEND_URL` | Url to redirect to  | https://medsafe.im        |

## License

Original development by [Johannah Sprinz](https://spri.nz). Copyright (C) 2020-2021 [ZiNK](https://zin-k.de).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
